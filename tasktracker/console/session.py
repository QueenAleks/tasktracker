from tasktracker.lib.actions.user_actions import user_exists

from getpass import getuser
import os

SESSION_INFO_FILE = os.path.join('/home', getuser(), '.ttrack_tracker', 'session.info')

class Session:
    def __init__(self, nickname=None):
        self.current_user = nickname
        if nickname is None and os.path.exists(SESSION_INFO_FILE):
            f = open(SESSION_INFO_FILE, 'r')
            n = f.readline()
            f.close()
            if user_exists(n):
                self.current_user = n
            else:
                os.remove(SESSION_INFO_FILE)
        elif nickname is not None:
            if os.path.exists(SESSION_INFO_FILE):
                os.remove(SESSION_INFO_FILE)
            f = open(SESSION_INFO_FILE, 'w')
            f.write(nickname)
            f.close()
