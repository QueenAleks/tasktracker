import tasktracker.console.handlers as handlers

import argparse
import datetime
import dateutil.parser

def create_task_parser(subparsers):
    task_parser = subparsers.add_parser('task', help='Provides operations with user tasks')
    task_subparsers = task_parser.add_subparsers(title='Operations with tasks')

    task_create_parser = task_subparsers.add_parser('create', help='Creates new task')
    task_create_parser.add_argument('title', help='task title')
    task_create_parser.add_argument('--description', help='task description', default='')
    task_create_parser.add_argument('--priority', '-p', help='task priority', choices=['FarFuture', 'Low', 'Normal', 'High', 'Extremely'], default='Normal')
    task_create_parser.add_argument('--date', '-d', help='deadline day (format: YYYY-MM-DD)')
    task_create_parser.add_argument('--time', '-t', help='deadline time (format: HH:MM)')
    task_create_parser.set_defaults(func=handlers.task.create)

    task_update_parser = task_subparsers.add_parser('update', help='Updates given info fields in existing task')
    task_update_parser.add_argument('task_id', help='id of task to update', type=int)
    task_update_parser.add_argument('--title', help='task title')
    task_update_parser.add_argument('--description', help='task description')
    task_update_parser.add_argument('--priority', '-p', help='task priority', choices=['FarFuture', 'Low', 'Normal', 'High', 'Extremely'], default=None)
    task_update_parser.add_argument('--date', '-d', help='deadline day (format: YYYY-MM-DD)')
    task_update_parser.add_argument('--time', '-t', help='deadline time (format: HH:MM)')
    task_update_parser.set_defaults(func=handlers.task.update)

    task_delete_parser = task_subparsers.add_parser('delete', help='Deletes existing task')
    task_delete_parser.add_argument('task_id', help='id of task to delete', type=int)
    task_delete_parser.set_defaults(func=handlers.task.delete)

    task_get_all_parser = task_subparsers.add_parser('get_all', help='Displays id and title off all user tasks')
    task_get_all_parser.set_defaults(func=handlers.task.get_all)

    task_get_info_parser = task_subparsers.add_parser('get_info', help='Displays full info about task')
    task_get_info_parser.add_argument('task_id', help='id of task to get info', type=int)
    task_get_info_parser.set_defaults(func=handlers.task.get_info)

    task_find_by_priority_parser = task_subparsers.add_parser('find_by_priority', help='Displays tasks with the given priority')
    task_find_by_priority_parser.add_argument('priority', help='task priority', choices=['FarFuture', 'Low', 'Normal', 'High', 'Extremely'])
    task_find_by_priority_parser.set_defaults(func=handlers.task.find_by_priority)

    task_find_by_status_parser = task_subparsers.add_parser('find_by_status', help='Displays tasks with the given status')
    task_find_by_status_parser.add_argument('status', help='task status', choices=['InProgress', 'Pending', 'Done', 'Failed'])
    task_find_by_status_parser.set_defaults(func=handlers.task.find_by_status)

    task_find_by_tag_parser = task_subparsers.add_parser('find_by_tag', help='Displays tasks with the given tag')
    task_find_by_tag_parser.add_argument('tag', help='task tag')
    task_find_by_tag_parser.set_defaults(func=handlers.task.find_by_tag)

    task_add_tag_parser = task_subparsers.add_parser('add_tag', help='Adds tag to given task')
    task_add_tag_parser.add_argument('task_id', help='task id', type=int)
    task_add_tag_parser.add_argument('tag', help='tag')
    task_add_tag_parser.set_defaults(func=handlers.task.add_tag)

    task_remove_tag_parser = task_subparsers.add_parser('remove_tag', help='Removes tag from given task')
    task_remove_tag_parser.add_argument('task_id', help='task id', type=int)
    task_remove_tag_parser.add_argument('tag', help='tag')
    task_remove_tag_parser.set_defaults(func=handlers.task.remove_tag)

    task_add_subtask_parser = task_subparsers.add_parser('add_subtask', help='Adds subtask with id \'subtask_id\' to task with \'task_id\'')
    task_add_subtask_parser.add_argument('task_id', help='id of parent task', type=int)
    task_add_subtask_parser.add_argument('subtask_id', help='id of new subtask', type=int)
    task_add_subtask_parser.set_defaults(func=handlers.task.add_subtask)

    task_get_task_tree_parser = task_subparsers.add_parser('get_task_tree', help='Displays suptasks tree of given task')
    task_get_task_tree_parser.add_argument('task_id', help='task id', type=int)
    task_get_task_tree_parser.set_defaults(func=handlers.task.get_task_tree)

    task_invite_mentor_parser = task_subparsers.add_parser('invite_mentor', help='Invites another user as mentor to given task')
    task_invite_mentor_parser.add_argument('task_id', help='task id', type=int)
    task_invite_mentor_parser.add_argument('nickname', help='nickname of mentor')
    task_invite_mentor_parser.set_defaults(func=handlers.task.invite_mentor)

    task_complete_parser = task_subparsers.add_parser('complete', help='Marks task as completed')
    task_complete_parser.add_argument('task_id', help='task id', type=int)
    task_complete_parser.set_defaults(func=handlers.task.complete)

def create_mentor_parser(subparsers):
    mentor_parser = subparsers.add_parser('mentor', help='Provides operations for mentors')
    mentor_subparsers = mentor_parser.add_subparsers(help='Operations for mentors')

    mentor_confirm_parser = mentor_subparsers.add_parser('confirm', help='Confirms that task was completed by owner')
    mentor_confirm_parser.add_argument('task_id', help='task id', type=int)
    mentor_confirm_parser.set_defaults(func=handlers.mentor.confirm)

    mentor_mark_failed_parser = mentor_subparsers.add_parser('mark_failed', help='Mark that task was failed by owner')
    mentor_mark_failed_parser.add_argument('task_id', help='task id', type=int)
    mentor_mark_failed_parser.set_defaults(func=handlers.mentor.mark_failed)

    mentor_get_info_parser = mentor_subparsers.add_parser('get_info', help='Displays full info about task that user mentored')
    mentor_get_info_parser.add_argument('task_id', help='task id', type=int)
    mentor_get_info_parser.set_defaults(func=handlers.mentor.get_info)

    mentor_get_tasks_parser = mentor_subparsers.add_parser('get_tasks', help='Displays all task where current user is a mentor')
    mentor_get_tasks_parser.set_defaults(func=handlers.mentor.get_tasks)

def create_routine_parser(subparsers):
    routine_parser = subparsers.add_parser('routine', help='Provides operations with routines')
    routine_subparsers = routine_parser.add_subparsers(help='Operations with routines')

    routine_create_parser = routine_subparsers.add_parser('create', help='Creates a routine from given task (task will be duplicated after given period)')
    routine_create_parser.add_argument('task_id', help='task id. Must have deadline day to create routine', type=int)
    routine_create_parser.add_argument('period', help='Periodicity of task creation', choices=['Daily', 'Weekly', 'Monthly', 'Yearly'])
    routine_create_parser.set_defaults(func=handlers.routine.create)

    routine_delete_parser = routine_subparsers.add_parser('delete', help='Deletes a routine from task (it will no longer be repeated)')
    routine_delete_parser.add_argument('task_id', help='task id', type=int)
    routine_delete_parser.set_defaults(func=handlers.routine.delete)

def create_reminder_parser(subparsers):
    reminder_parser = subparsers.add_parser('reminder', help='Provides operations with reminders')
    reminder_subparsers = reminder_parser.add_subparsers(help='Operations with routines')

    reminder_create_parser = reminder_subparsers.add_parser('create', help='Creates a reminder for given task')
    reminder_create_parser.add_argument('task_id', help='task id', type=int)
    reminder_create_parser.add_argument('datetime', help='date and time when the reminder will triggered (format: YYYY-MM-DD HH:MM)')
    reminder_create_parser.add_argument('--note', '-n', help='some notes for reminder')
    reminder_create_parser.set_defaults(func=handlers.reminder.create)

    reminder_delete_parser = reminder_subparsers.add_parser('delete', help='Deletes reminder with id \'reminder_id\'')
    reminder_delete_parser.add_argument('reminder_id', help='id of reminder to delete')
    reminder_delete_parser.set_defaults(func=handlers.reminder.delete)

    reminder_get_all_parser = reminder_subparsers.add_parser('get_all', help='Displays all triggered user reminders')
    reminder_get_all_parser.set_defaults(func=handlers.reminder.get_all)

def create_project_parser(subparsers):
    project_parser = subparsers.add_parser('project', help='Provides operations with projects')
    project_subparsers = project_parser.add_subparsers(help='Operations with projects')

    project_create_parser = project_subparsers.add_parser('create', help='Creates a new project. User will have \'Organizer\' role.')
    project_create_parser.add_argument('name', help='new project name')
    project_create_parser.set_defaults(func=handlers.project.create)

    project_rename_parser = project_subparsers.add_parser('rename', help='Renames existing project. Allowed only to the \'Organizer\'.')
    project_rename_parser.add_argument('project_id', help='id of project to rename', type=int)
    project_rename_parser.add_argument('name', help='new name of existing project')
    project_rename_parser.set_defaults(func=handlers.project.rename)

    project_delete_parser = project_subparsers.add_parser('delete', help='Deletes project. Allowed only to the \'Organizer\'.')
    project_delete_parser.add_argument('project_id', help='Project id', type=int)
    project_delete_parser.set_defaults(func=handlers.project.delete)

    project_get_all_parser = project_subparsers.add_parser('get_all', help='Displays all projects in which the user is.')
    project_get_all_parser.set_defaults(func=handlers.project.get_all)

    project_find_by_role_parser = project_subparsers.add_parser('find_by_role', help='Displays all projects in which user has a given role')
    project_find_by_role_parser.add_argument('role', help='role in project', choices=['Organizer', 'Manager', 'Partner'])
    project_find_by_role_parser.set_defaults(func=handlers.project.find_by_role)

    project_get_tasks_parser = project_subparsers.add_parser('get_tasks', help='Displays all tasks in the given project')
    project_get_tasks_parser.add_argument('project_id', help='project id', type=int)
    project_get_tasks_parser.set_defaults(func=handlers.project.get_tasks)

    project_get_members_parser = project_subparsers.add_parser('get_members', help='Displays all members of the given project')
    project_get_members_parser.add_argument('project_id', help='project id', type=int)
    project_get_members_parser.set_defaults(func=handlers.project.get_members)

    project_complete_task_parser = project_subparsers.add_parser('complete_task', help='Mark task as completed. All members of project can do this.')
    project_complete_task_parser.add_argument('project_id', help='project id', type=int)
    project_complete_task_parser.add_argument('task_id', help='task id', type=int)
    project_complete_task_parser.set_defaults(func=handlers.project.complete_task)

    project_add_task_parser = project_subparsers.add_parser('add_task', help='Adds task to project. Allowed to \'Managers\' and \'Organizers\'.')
    project_add_task_parser.add_argument('project_id', help='project id', type=int)
    project_add_task_parser.add_argument('task_id', help='task id', type=int)
    project_add_task_parser.set_defaults(func=handlers.project.add_task)

    project_remove_task_parser = project_subparsers.add_parser('remove_task', help='Removes task from project (but not deletes at all). Allowed to \'Managers\' and \'Organizers\'.')
    project_remove_task_parser.add_argument('project_id', help='project id', type=int)
    project_remove_task_parser.add_argument('task_id', help='task id', type=int)
    project_remove_task_parser.set_defaults(func=handlers.project.remove_task)

    project_invite_member_parser = project_subparsers.add_parser('invite_member', help='Invites user with given nickname to given project as a \'Partner\'. Allowed to \'Managers\' and \'Organizers\'.')
    project_invite_member_parser.add_argument('project_id', help='project id', type=int)
    project_invite_member_parser.add_argument('nickname', help='nickname of user to invite')
    project_invite_member_parser.set_defaults(func=handlers.project.invite_member)

    project_exclude_member_parser = project_subparsers.add_parser('exclude_member', help='Excludes member from project. Allowed to \'Managers\' and \'Organizers\'. \'Organizer\' can\'t be excluded. \'Managers\' can\'t exclude other \'Managers\'.')
    project_exclude_member_parser.add_argument('project_id', help='project id', type=int)
    project_exclude_member_parser.add_argument('nickname', help='nickname of user to exclude')
    project_exclude_member_parser.set_defaults(func=handlers.project.exclude_member)

    project_appoint_manager_parser = project_subparsers.add_parser('appoint_manager', help='Appoint given \'Partner\' as \'Manager\' for given project.')
    project_appoint_manager_parser.add_argument('project_id', help='project id', type=int)
    project_appoint_manager_parser.add_argument('nickname', help='nickname of user to appoint')
    project_appoint_manager_parser.set_defaults(func=handlers.project.appoint_manager)

    project_dismiss_manager_parser = project_subparsers.add_parser('dismiss_manager', help='Makes the \'Manager\' a \'partner\'.')
    project_dismiss_manager_parser.add_argument('project_id', help='project id', type=int)
    project_dismiss_manager_parser.add_argument('nickname', help='nickname of user to dismiss')
    project_dismiss_manager_parser.set_defaults(func=handlers.project.dismiss_manager)



def parse():
    parser = argparse.ArgumentParser(description='ttrack console application')
    subparsers = parser.add_subparsers(title='commands for ttrack application')

    create_user_parser = subparsers.add_parser('create_user', help='Creates new user account')
    create_user_parser.add_argument('nickname', help='User nickname. Choose wisely! You won\'t be able to change it after creation.')
    create_user_parser.set_defaults(func=handlers.create_user)

    delete_user_parser = subparsers.add_parser('delete_user', help='Deletes current user account. Attention! You will not be able to restore it.')
    delete_user_parser.set_defaults(func=handlers.delete_user)

    login_parser = subparsers.add_parser('login', help='Log in to the application with the selected account')
    login_parser.add_argument('nickname', help='User account nickname')
    login_parser.set_defaults(func=handlers.login)

    whoami_parser = subparsers.add_parser('whoami', help='Displays the name of the current user')
    whoami_parser.set_defaults(func=handlers.whoami)

    get_tags_parser = subparsers.add_parser('get_tags', help='Displays all user tags')
    get_tags_parser.set_defaults(func=handlers.get_tags)

    delete_tag_parser = subparsers.add_parser('delete_tag', help='Delete tag from all user tasks')
    delete_tag_parser.add_argument('tag', help='Tag to delete')
    delete_tag_parser.set_defaults(func=handlers.delete_tag)

    task_parser = create_task_parser(subparsers)

    mentor_parser = create_mentor_parser(subparsers)

    routine_parser = create_routine_parser(subparsers)

    reminder_parser = create_reminder_parser(subparsers)

    project_parser = create_project_parser(subparsers)

    return parser.parse_args()