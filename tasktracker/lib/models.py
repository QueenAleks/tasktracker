'''
Models for library based on Django ORM.
'''

from django.db import models

class User(models.Model):
    nickname = models.CharField(max_length=30, primary_key=True)
    karma = models.IntegerField(default=0)

    mentors = models.ManyToManyField('self', through='Task', blank=True, symmetrical=False)

    def __str__(self):
        return self.nickname


class Task(models.Model):
    PRIORITY_CHOISES = (
        (0, 'FarFuture'),
        (1, 'Low'),
        (2, 'Normal'),
        (3, 'High'),
        (4, 'Extremely'),
    )
    STATUS_CHOISES = (
        (0, 'InProgress'),
        (1, 'Pending'),
        (2, 'Done'),
        (3, 'Failed'),
    )
    title = models.CharField(max_length=100)
    description = models.TextField(max_length=2000)
    priority = models.IntegerField(choices=PRIORITY_CHOISES, default=2)
    status = models.IntegerField(choices=STATUS_CHOISES, default=0)
    deadline_day = models.DateField(null=True)
    deadline_time = models.TimeField(null=True)
    
    owner = models.ForeignKey(User, related_name='owner_tasks', on_delete=models.CASCADE)
    mentor = models.ForeignKey(User, null=True, default=None, related_name='mentor_tasks', on_delete=models.SET_NULL)
    parent_task = models.ForeignKey('self', null=True, default=None, on_delete=models.CASCADE)
    project = models.ForeignKey('Project', null=True, default=None, on_delete=models.CASCADE)
    tags = models.ManyToManyField('Tag', blank=True)

    def __str__(self):
        return '#{}: {}{}'.format(self.id, self.title, '' if not self.task_set.exists() else ' (has subtasks)')


class Tag(models.Model):
    name = models.CharField(max_length=20, primary_key=True)

    def __str__(self):
        return self.name

class Routine(models.Model):
    next_datetime = models.DateTimeField()
    PERIOD_CHOICE = (
        (0, 'Daily'),
        (1, 'Weekly'),
        (2, 'Monthly'),
        (3, 'Yearly'),
    )
    period = models.IntegerField(choices=PERIOD_CHOICE)

    next_task = models.OneToOneField(Task, on_delete=models.CASCADE)


class Reminder(models.Model):
    at_time = models.DateTimeField()
    note = models.CharField(max_length=100, null=True)

    task = models.ForeignKey(Task, on_delete=models.CASCADE)

    def __str__(self):
        return f'Task #{self.task.id}: {self.at_time}: {self.note}'


class Project(models.Model):
    name = models.CharField(max_length=100)

    users = models.ManyToManyField(User, through='UserRole')

    def __str__(self):
        return f'#{self.id}: {self.name}'


class UserRole(models.Model):
    ROLE_CHOISES = (
        (0, 'Organizer'),
        (1, 'Manager'),
        (2, 'Partner'),
    )
    role = models.IntegerField(choices=ROLE_CHOISES)

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
