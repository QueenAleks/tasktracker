from django.test import TestCase

import tasktracker.lib.actions.tag_actions as tag
import tasktracker.lib.exceptions as exceptions
import tasktracker.lib.models as models

class TagActionsTestCase(TestCase):
    def setUp(self):
        u = models.User(nickname='kek')
        u.save()
        self.tag = []
        for i in range(3):
            self.tag.append(models.Tag(name=f't{i}'))
            self.tag[i].save()
        self.task = []
        for i in range(2):
            self.task.append(models.Task(title=f'test{i}', owner=u))
            self.task[i].save()
            self.task[i].tags.add(self.tag[i])
            self.task[i].tags.add(self.tag[2])

    def test_get_tasks_by_tag(self):
        self.assertEqual(tag.get_tasks_by_tag('kek', 't0'), [(self.task[0].id, self.task[0].title, 0)])
        self.assertEqual(tag.get_tasks_by_tag('kek', 't1'), [(self.task[1].id, self.task[1].title, 0)])
        self.assertEqual(tag.get_tasks_by_tag('kek', 't2'), [(self.task[0].id, self.task[0].title, 0), (self.task[1].id, self.task[1].title, 0)])

    def test_get_tags(self):
        tags = tag.get_tags('kek')
        self.assertIn('t0', tags)
        self.assertIn('t1', tags)
        self.assertIn('t2', tags)
    
    def test_delete_tag(self):
        tag.delete_tag('kek', 't2')
        with self.assertRaises(models.Tag.DoesNotExist):
            self.task[0].tags.get(name='t2')
            self.   task[1].tags.get(name='t2')