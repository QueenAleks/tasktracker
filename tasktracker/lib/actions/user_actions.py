'''
Provides actions with User accounts.
'''

import tasktracker.lib.models as models
import tasktracker.lib.exceptions as exceptions
from tasktracker.lib.logger import get_Logger

import re
import logging

def create_user(name=''):
    '''
    Creates new user account in database.
    '''
    logger = get_Logger(__name__)
    try:
        if re.fullmatch('^\s*$', name):
            raise ValueError('Name can\'t be blank')
        if len(name) > 30:
            raise ValueError('User nickname too long')
        models.User.objects.get(nickname=name)
        logger.error(f'user {name} already exists')
        raise exceptions.NotAllowedException("User already exists")
    except models.User.DoesNotExist:
        models.User(nickname=name).save()
        logger.info(f'User  {name} created')
    except ValueError:
        logger.error('Create user. Value error')
        raise

def delete_user(name=''):
    '''
    Deletes user account from database.
    '''
    logger = get_Logger(__name__)
    try:
        models.User.objects.get(nickname=name).delete()
        logger.info(f'User {name} deleted')
    except models.User.DoesNotExist:
        logger(f'User {name} already doesn\'t exist')

def user_exists(name):
    '''
    Returns True if account 'name' exists,
    and False if not.
    '''
    try:
        models.User.objects.get(nickname=name)
        return True
    except models.User.DoesNotExist:
        return False