from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import Http404

from tasktracker.web.view_decorators import do_background_jobs
from tasktracker.lib.core import reminder
from tasktracker.lib.core import routine
import tasktracker.lib.models as models
from tasktracker.lib.core import task
import tasktracker.web.forms as forms

PRIORITIES = {
    'FarFuture': 0,
    'Low': 1,
    'Normal': 2,
    'High': 3,
    'Extremely': 4
}

@login_required
@do_background_jobs
def task_page(request, task_id):
    task_id = int(task_id)
    if not task.task_exists(request.user.username, task_id):
        raise Http404()
    
    form = None
    if request.method == 'POST':
        form = forms.TaskForm(request.POST)
        if form.is_valid():
            task.update_task(
                request.user.username,
                task_id,
                title=request.POST['title'],
                description=request.POST['description'],
                priority=form.cleaned_data['priority'],
                day=form.cleaned_data['deadline_day'],
                time=form.cleaned_data['deadline_time']
            )
            return redirect(f'/task/{task_id}/')
    else:
        task_item = models.Task.objects.filter(id=task_id)[0]
        form = forms.TaskForm(initial={
            'title': task_item.title,
            'description': task_item.description,
            'priority': task_item.priority,
            'deadline_day': task_item.deadline_day,
            'deadline_time': task_item.deadline_time
        })

    info = task.get_task_info(request.user.username, task_id)
    this_task, subtasks = task.get_task_tree(request.user.username, task_id)
    subtasks = [t_info for t_info, sub_subtree in subtasks]
    task_routine = None
    try:
        task_routine = models.Task.objects.get(id=task_id).routine
    except:
        task_routine = None
    data = {
        'form': form,
        'nickname': request.user.username,
        'task_id': task_id,
        'task_info': info,
        'tags': task.get_tags(request.user.username, task_id),
        'tag_add_form': forms.TagAddForm(),
        'subtasks': subtasks,
        'create_task_form': forms.TaskCreateForm(),
        'parent_task': info['parent_task_id'],
        'period': None if task_routine is None else task_routine.period,
        'project': info['project_id']
    }
    return render(request, 'task_page.html', data)

@login_required
def task_delete(request, task_id):
    task_id = int(task_id)
    if not task.task_exists(request.user.username, task_id):
        raise Http404()
    else:
        task.delete_task(request.user.username, task_id)
        return redirect(f'/user/{request.user.username}/')

@login_required
@require_POST
def task_create(request):
    form = forms.TaskCreateForm(request.POST)
    if form.is_valid():
        task.create_task(request.user.username, request.POST['title'])
        return redirect(f'/user/{request.user.username}/')
    else:
        return HttpResponse(form.fieldname.errors.as_text)
    

@login_required
@require_POST
def task_add_subtask(request, parent_task_id):
    form = forms.TaskCreateForm(request.POST)
    if form.is_valid():
        subtask = task.create_task(request.user.username, form.cleaned_data['title'])
        task.add_subtask(request.user.username, parent_task_id, subtask)
        return redirect(f'/task/{parent_task_id}/')
    else:
        return HttpResponse(form.fieldname.errors.as_text)

@login_required
@require_POST
def task_add_tag(request, task_id):
    form = forms.TagAddForm(request.POST)
    if form.is_valid():
        task.add_tag(request.user.username, int(task_id), request.POST['tag'])
        return redirect(f'/task/{task_id}/')
    else:
        return HttpResponse(form.fieldname.errors.as_text)

@login_required
def task_remove_tag(request, task_id, tag):
    task.delete_tag(request.user.username, task_id, tag)
    return redirect(f'/task/{task_id}/')

@login_required
def task_complete(request, task_id):
    try:
        task.mark_as_completed(request.user.username, task_id)
        return redirect(f'/user/{request.user.username}/')
    except Exception as ex:
        return HttpResponse(ex.args[0])

@login_required
@require_POST
def task_invite_mentor(request):
    try:
        task.invite_mentor(request.user.username, request.POST['task_id'], request.POST['mentor'])
        return HttpResponse('ok')
    except Exception as ex:
        return HttpResponse(ex.args[0])

@login_required
@require_POST
def task_select_period(request, task_id):
    try:
        routine.delete_routine(request.user.username, int(task_id))
    except:
        pass
    if request.POST['period'] == 'None':
        return redirect(f'/task/{task_id}/')
    period = int(request.POST['period'])
    routine.create_routine(request.user.username, task_id, period)
    return redirect(f'/task/{task_id}/')

@login_required
@require_POST
def task_create_reminder(request):
    form = forms.ReminderForm(request.POST)
    if form.is_valid():
        reminder.create_reminder(request.user.username, request.POST['task_id'], request.POST['at_time'], request.POST['note'])
        return HttpResponse('ok')
    else:
        return HttpResponse('bad')

@login_required
@require_POST
def task_delete_reminder(request):
    reminder.delete_reminder(request.user.username, request.POST['reminder_id'])
    return HttpResponse('ok')

@login_required
@require_POST
def mentor_confirm(request):
    task.mentor_confirm_completion(request.user.username, request.POST['task_id'])
    return HttpResponse('ok')

@login_required
@require_POST
def mentor_mark_failed(request):
    task.mentor_mark_failed(request.user.username, request.POST['task_id'])
    return HttpResponse('ok')