from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.http import Http404

from tasktracker.web.view_decorators import do_background_jobs
from tasktracker.lib.core import background
from tasktracker.lib.core import project
from tasktracker.lib.core import user
from tasktracker.lib.core import task
import tasktracker.web.forms as forms

def home_page(request):
    if request.user.is_authenticated:
        return redirect(f'/user/{request.user.username}')
    else:
        return render(request, 'home_page.html')

def register(request):
    form = None
    if request.method == 'POST':
        form = forms.RegistrationForm(request.POST)
        if form.is_valid():
            nickname = form.cleaned_data['nickname']
            password = form.cleaned_data['password']
            User.objects.create_user(username=nickname, password=password)
            user.create_user(name=nickname)
            new_user = authenticate(request, username=nickname, password=password)
            login(request, new_user)
            return redirect(f'/user/{nickname}')
    else:
        form = forms.RegistrationForm()
    return render(request, 'registration_page.html', {'form': form})

def login_view(request):
    if request.user.is_authenticated:
        logout(request)
    form = None
    if request.method == 'POST':
        form = forms.LoginForm(request.POST)
        if form.is_valid():
            nickname = form.cleaned_data['nickname']
            password = form.cleaned_data['password']
            new_user = authenticate(request, username=nickname, password=password)
            login(request, new_user)
            return redirect(f'/user/{nickname}')
    else:
        form = forms.LoginForm()
    return render(request, 'login_page.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect('/')

@login_required
@do_background_jobs
def user_page(request, nickname=None):
    if request.user.username != nickname:
        raise Http404()
    
    data = {
        'nickname': nickname,
        'reminders': background.get_reminders(request.user.username),
        'failed_tasks': background.check_deadlines(request.user.username),
        'user_tasks': task.get_user_tasks(request.user.username),
        'mentored_tasks': task.get_mentor_tasks(request.user.username),
        'projects': project.get_user_projects(request.user.username),
        'create_task_form': forms.TaskCreateForm(),
        'create_project_form': forms.ProjectCreateForm()
    }
    return render(request, 'user_page.html', data)

@login_required
def user_delete(request, nickname):
    if request.user.username != nickname:
        raise Http404()
    
    User.objects.get(username=request.user.username).delete()
    user.delete_user(name=request.user.username)
    logout(request)
    return redirect('/')