from django.forms import ModelForm, DateInput, TimeInput
from django.forms import modelformset_factory
from django.contrib.auth.models import User
from django.forms import BaseModelFormSet
from django import forms

import tasktracker.lib.models as models

import re
import logging
import datetime

class RegistrationForm(forms.Form):
    nickname = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput())

    def clean(self):
        if self.cleaned_data.get('nickname') is None:
            raise forms.ValidationError('Nickname can\'t be blank')
        if User.objects.filter(username=self.cleaned_data.get('nickname')).exists():
            raise forms.ValidationError('This nickname is already picked')
        if not re.fullmatch('[a-zA-Z0-9]+', self.cleaned_data.get('nickname')):
            raise forms.ValidationError('Unacceptable symbols')
        return self.cleaned_data

class LoginForm(forms.Form):
    nickname = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput())

    def clean(self):
        if self.cleaned_data.get('nickname') is None:
            raise forms.ValidationError('Nickname can\'t be blank')
        if not re.fullmatch('[a-zA-Z0-9]+', self.cleaned_data.get('nickname')):
            raise forms.ValidationError('Unacceptable symbols')
        if not models.User.objects.filter(nickname=self.cleaned_data.get('nickname')).exists():
            raise forms.ValidationError('Account doesn\'t exist')
        return self.cleaned_data

class TaskForm(ModelForm):
    class Meta:
        model = models.Task
        fields = ['title', 'description', 'priority', 'deadline_day', 'deadline_time']

    def clean(self):
        date = self.cleaned_data.get('deadline_day')
        time = self.cleaned_data.get('deadline_time')
        if self.cleaned_data.get('title') is None:
            raise forms.ValidationError('Title can\'t be blank')
        if date is not None and date < datetime.date.today():
            raise forms.ValidationError('Deadline can\'t be in past')
        if time is not None:
            if date is None:
                raise forms.ValidationError('You have to select day to select time')
            if datetime.datetime.combine(date, time) <= datetime.datetime.now():
                raise forms.ValidationError('Deadline can\'t be in past')
        return self.cleaned_data

class TaskCreateForm(forms.Form):
    title = forms.CharField(max_length=100)

    def clean(self):
        if self.cleaned_data.get('title') is None:
            raise forms.ValidationError('Title can\'t be blank')
        return self.cleaned_data

class TagAddForm(forms.Form):
    tag = forms.CharField(max_length=20)

    def clean(self):
        if self.cleaned_data.get('tag') is None:
            raise forms.ValidationError('Title can\'t be blank')
        return self.cleaned_data

class ReminderForm(ModelForm):
    class Meta:
        model = models.Reminder
        fields = ['at_time', 'note']

class ProjectCreateForm(forms.Form):
    name = forms.CharField(max_length=100)

    def clean(self):
        if self.cleaned_data.get('name') is None:
            raise forms.ValidationError('Name can\'t be blank')
        return self.cleaned_data

class UserInviteForm(forms.Form):
    nickname = forms.CharField(max_length=30)

    def clean(self):
        if not models.User.objects.filter(nickname=self.cleaned_data['nickname']).exists():
            raise forms.ValidationError('User doesn\'t exists.')
        
