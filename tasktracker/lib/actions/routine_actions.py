'''
Provides actions with Routines.
'''

import tasktracker.lib.models as models
from tasktracker.lib.logger import get_Logger

from dateutil.relativedelta import relativedelta
import datetime
import logging

PERIOD_DELTAS = {
    0: relativedelta(days=+1),
    1: relativedelta(weeks=+1),
    2: relativedelta(months=+1),
    3: relativedelta(years=+1)
}

def create_routine(user, task_id, period):
    '''
    Creates routine.
    '''
    logger = get_Logger(__name__)
    task = None
    try:
        task = models.User.objects.get(nickname=user).owner_tasks.get(id=task_id)
    except models.Task.DoesNotExist:
        logger.error(f'Task {task_id} doesn\'t exist')
        raise
    if PERIOD_DELTAS.get(period) is None:
        logger.error(f'period param must be 0, 1, 2 or 3')
        raise ValueError(f'period param must be 0, 1, 2 or 3')
    day = task.deadline_day
    time = task.deadline_time
    if day is None:
        logger.error(f'Task.deadline_day must not be empty to create a routine for task')
        raise ValueError(f'Task.deadline_day must not be empty to create a routine for task')
    if time is None:
        time = datetime.time(0,0,0)
    models.Routine(period=period, next_task=task, next_datetime=datetime.datetime.combine(day, time)).save()
    logger.info(f'User {user} created routine for task {task_id}')

def delete_routine(user, task_id):
    '''
    Deletes Routine.
    '''
    logger = get_Logger(__name__)
    try:
        models.User.objects.get(nickname=user).owner_tasks.get(id=task_id).routine.delete()
    except models.Task.DoesNotExist:
        logger.error(f'Task {task_id} doesn\'t exist')
        raise
    except models.Routine.DoesNotExist:
        logger.error(f'Task {task_id} doesn\'t have a routine')

