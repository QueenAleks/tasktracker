'''
Provides actions, that should run in background.
'''

import tasktracker.lib.models as models
from tasktracker.lib.logger import get_Logger
import tasktracker.lib.actions.routine_actions as routine

from dateutil.relativedelta import relativedelta
import datetime
import logging

PERIOD_DELTAS = {
    0: relativedelta(days=+1),
    1: relativedelta(weeks=+1),
    2: relativedelta(months=+1),
    3: relativedelta(years=+1)
}

STATUSES = {
    0: 'InProgress',
    1: 'Pending',
    2: 'Done',
    3: 'Failed'
}

def get_reminders(user):
    '''
    Returns reminders, in which 'at_time' is lower that current time.
    '''
    reminders = []
    for t in models.User.objects.get(nickname=user).owner_tasks.all().prefetch_related('reminder_set'):
        l = [(r.task.id, r.at_time, r.note) for r in t.reminder_set.filter(at_time__lte=datetime.datetime.now())]
        reminders.extend(l)
    return reminders

def make_routine_tasks():
    '''
    Existing Routine's of all User's dublicates associated Task's with given period
    if 'next_time' is lower than current time.
    '''
    routines = models.Routine.objects.filter(next_datetime__lte=datetime.datetime.now()).prefetch_related('next_task')
    for r in routines:
        t = r.next_task
        old_tags = t.tags.all()
        old_reminders = t.reminder_set.all()
        t.pk = None
        t.deadline_day += PERIOD_DELTAS[r.period]
        t.status = 0
        t.save()
        t.tags.set(old_tags)
        new_reminders = []
        for rem in old_reminders:
            rem.pk = None
            rem.at_time += PERIOD_DELTAS[r.period]
            rem.save()
            rem.task = t
            rem.save()
            new_reminders.append(rem)
        t.reminder_set.set(new_reminders)
        t.save()
        routine.create_routine(t.owner.nickname, t.id, r.period)
        r.delete()

def check_deadlines(user):
    '''
    Checks if Task's of given User have deadlines in past,
    than marked 'status' of that tasks as 'Failed'
    and returns id's of failed tasks.
    '''
    logger = get_Logger(__name__)
    fails = []
    tasks = (models.User.objects.get(nickname=user).owner_tasks.filter(deadline_day__lte=datetime.date.today())    
                              .filter(deadline_time__lt=datetime.datetime.time(datetime.datetime.now())))
    for t in list(tasks):
        if t.get_status_display() == STATUSES[0]:
            t.status = 3
            t.save()
            logger.info(f'User {t.owner} failed task {t.id}')
            if t.owner.nickname == user:
                fails.append((t.id, t.title))
    return fails
