from django.test import TestCase

import tasktracker.lib.actions.routine_actions as routine
import tasktracker.lib.exceptions as exceptions
import tasktracker.lib.models as models

import datetime

class RoutineActionsTestCase(TestCase):
    def setUp(self):
        u = models.User(nickname='kek')
        u.save()
        self.task1 = models.Task(deadline_day=datetime.datetime.now().date(), deadline_time=datetime.datetime.now().time(), title=f'test1', owner=u)
        self.task2 = models.Task(deadline_day=datetime.datetime.now().date(), deadline_time=datetime.datetime.now().time(), title=f'test2', owner=u)
        self.task3 = models.Task(title=f'test3', owner=u)
        self.task1.save()
        self.task2.save()
        self.task3.save()
        r_datetime = datetime.datetime.combine(self.task1.deadline_day, self.task1.deadline_time)
        models.Routine(period=0, next_task=self.task1, next_datetime=r_datetime).save()
    
    def test_creation(self):
        routine.create_routine('kek', self.task2.id, 0)
        self.assertIn(models.Routine.objects.get(next_task=self.task2), models.Routine.objects.all())

    def test_creation__incorrect_period(self):
        with self.assertRaises(ValueError):
            routine.create_routine('kek', self.task2.id, None)
            routine.create_routine('kek', self.task2.id, 5)
            routine.create_routine('kek', self.task2.id, -1)

    def test_creation__without_deadline(self):
        with self.assertRaises(ValueError):
            routine.create_routine('kek', self.task3.id, 0)

    def test_creation__task_does_not_exist(self):
        with self.assertRaises(models.Task.DoesNotExist):
            routine.create_routine('kek', -1, 0)

    def test_delete(self):
        routine.delete_routine('kek', self.task1.id)
        self.assertFalse(self.task1.reminder_set.all().exists())

    def test_delete__without_deadline(self):
        with self.assertRaises(models.Task.DoesNotExist):
            routine.delete_routine('kek', -1)