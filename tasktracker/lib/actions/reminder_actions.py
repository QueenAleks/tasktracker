'''
Provides actions with reminders.
'''

import tasktracker.lib.models as models
from tasktracker.lib.logger import get_Logger

import logging

def create_reminder(user, task_id, at_time, note=None):
    '''
    Creates new Reminder.
    '''
    logger = get_Logger(__name__)
    r = None
    try:
        t = models.User.objects.get(nickname=user).owner_tasks.get(id=task_id)
        r = models.Reminder(at_time=at_time, note=note, task=t)
        r.save()
        logger.info(f'User {user} added reminder {r.id} to task {task_id}')
    except models.Task.DoesNotExist:
        logger.error(f'Task {task_id} doesn\'t exist')
        raise
    return r.id

def delete_reminder(user, id):
    '''
    Deletes Reminder with given id.
    '''
    logger = get_Logger(__name__)
    try:
        models.Reminder.objects.get(id=id).delete()
        logger.info(f'User {user} deleted reminder {id}')
    except models.Reminder.DoesNotExist:
        logger.error(f'Reminder {id} doesn\'t exist')
