'''
Provides actions with Tags.
'''

import tasktracker.lib.models as models
from tasktracker.lib.logger import get_Logger

import logging

def get_tasks_by_tag(user, tag):
    '''
    Returns Task's which has given tag.
    '''
    selected_tag = None
    try:
        selected_tag = models.Tag.objects.get(name=tag)
    except models.Tag.DoesNotExist:
        return []
    tasks = selected_tag.task_set.filter(owner__nickname=user)
    return [(t.id, t.title, t.status) for t in tasks]

def get_tags(user):
    '''
    Returns all Tag's of current User.
    '''
    tags = []
    for task in models.User.objects.get(nickname=user).owner_tasks.all():
        tags.extend([str(t) for t in task.tags.all()])
    return list(set(tags))

def delete_tag(user, tag):
    '''
    Deletes given Tag from all Task's of given User.
    '''
    t = models.User.objects.get(nickname=user).owner_tasks.all().prefetch_related('tags')
    for task in t:
        try:
            tg = task.tags.get(name=tag)
            task.tags.remove(tg)
        except models.Tag.DoesNotExist:
            pass
    logger = get_Logger(__name__)
    logger.info(f'User {user} deleted tag {tag}')