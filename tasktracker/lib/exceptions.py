class NotAllowedException(Exception):
    pass

class ProjectAccessException(Exception):
    pass