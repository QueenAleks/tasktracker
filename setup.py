from setuptools import setup, find_packages
import os

setup(
    name='ttrack-tracker',
    version='0.666',
    description='To-Do application',
    long_description=open('README.md').read(),
    packages=find_packages(),
    url='https://bitbucket.org/QueenAleks/tasktracker',
    install_requires=['django==1.11','mysqlclient'],
    entry_points={
        'console_scripts': [
            'ttrack=tasktracker.console.ttrack:main',
            'ttrack-setup=tasktracker.console.ttrack:setup'
        ]
    },
    author='Aleksey Pesenko',
    author_email='MrTwisterOriginal@gmail.com'
)