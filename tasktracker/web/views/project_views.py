from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import Http404

from tasktracker.web.view_decorators import do_background_jobs
from tasktracker.lib.core import user_role
from tasktracker.lib.core import project
import tasktracker.lib.models as models
from tasktracker.lib.core import user
from tasktracker.lib.core import task
import tasktracker.web.forms as forms

ROLES = {
    0: 'Organizer',
    1: 'Manager',
    2: 'Partner'
}

@login_required
@do_background_jobs
def project_page(request, project_id):
    project_id = int(project_id)
    if not models.User.objects.get(nickname=request.user.username).project_set.get(id=project_id):
        raise Http404()

    tasks = []
    for id, title, status in project.get_project_tasks(request.user.username, project_id):
        tasks.append((id, title, status, models.Task.objects.get(id=id).owner.nickname))
    data = {
        'project_id': project_id,
        'project_name': models.Project.objects.get(id=project_id).name,
        'nickname': request.user.username,
        'role': project.get_project_role(request.user.username, project_id),
        'tasks': tasks,
        'members': project.get_members(project_id),
        'create_task_form': forms.TaskCreateForm(),
        'invite_user_form': forms.UserInviteForm()
    }
    return render(request, 'project_page.html', data)

@login_required
def project_delete(request, project_id):
    project_id = int(project_id)
    if not models.User.objects.get(nickname=request.user.username).project_set.get(id=project_id):
        raise Http404()

    if project.get_project_role(request.user.username, project_id) == ROLES[0]:
        project.delete_project(request.user.username, project_id)
        return redirect(f'/user/{request.user.username}/')
    else:
        raise Http404()

@login_required
@require_POST
def project_create(request):
    form = forms.ProjectCreateForm(request.POST)
    if form.is_valid():
        answer = project.create_project(request.user.username, request.POST['name'])
        return redirect(f'/project/{answer}/')
    else:
        return HttpResponse(form.fieldname.errors.as_text)

@login_required
def project_complete_task(request, project_id, task_id):
    user_role.complete_task(request.user.username, project_id, task_id)
    return redirect(f'/project/{project_id}/')

@login_required
@require_POST
def project_add_task(request, project_id):
    form = forms.TaskCreateForm(request.POST)
    if form.is_valid():
        task_id = task.create_task(request.user.username, form.cleaned_data['title'])
        user_role.add_task_to_project(request.user.username, project_id, task_id)
        return redirect(f'/project/{project_id}/')
    else:
        return HttpResponse(form.fieldname.errors.as_text)

@login_required
def project_remove_task(request, project_id, task_id):
    user_role.remove_task_from_project(request.user.username, project_id, task_id)
    task.delete_task(request.user.username, task_id)
    return redirect(f'/project/{project_id}/')

@login_required
@require_POST
def project_invite(request, project_id):
    form = forms.UserInviteForm(request.POST)
    if form.is_valid():
        if user.user_exists(form.cleaned_data['nickname']):
            user_role.invite_partner(request.user.username, form.cleaned_data['nickname'], project_id)
            return redirect(f'/project/{project_id}/')
        else:
            return HttpResponse('bad')
    else:
        tasks = []
        for id, title, status in project.get_project_tasks(request.user.username, project_id):
            tasks.append((id, title, status, models.Task.objects.get(id=id).owner.nickname))
        data = {
            'project_id': project_id,
            'project_name': models.Project.objects.get(id=project_id).name,
            'nickname': request.user.username,
            'role': project.get_project_role(request.user.username, project_id),
            'tasks': tasks,
            'members': project.get_members(project_id),
            'create_task_form': forms.TaskCreateForm(),
            'invite_user_form': form
        }
        return render(request, 'project_page.html', data)

@login_required
def project_exclude(request, project_id, nickname):
    if user.user_exists(nickname):
        user_role.exclude_partner(request.user.username, nickname, project_id)
        return redirect(f'/project/{project_id}/')
    else:
        return HttpResponse('bad')

@login_required
def project_appoint_manager(request, project_id, nickname):
    user_role.appoint_as_manager(request.user.username, nickname, project_id)
    return redirect(f'/project/{project_id}/')

@login_required
def project_dismiss_manager(request, project_id, nickname):
    user_role.dismiss_manager(request.user.username, nickname, project_id)
    return redirect(f'/project/{project_id}/')