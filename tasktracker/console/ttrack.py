import tasktracker.config as conf
import tasktracker.lib.core as core
from tasktracker.console.parser import parse
from tasktracker.console.session import Session
from tasktracker.console.background import do_jobs



def setup():
    core.setup_database(conf.DATABASE_CONFIG)

def main():
    core.init(logger_config=conf.LOGGING_CONFIG)
    session = Session()
    args = parse()
    if session.current_user is not None:
        do_jobs(session.current_user)
    answer = args.func(args, session.current_user)
    if type(answer) is Session:
        session = answer



if __name__ == '__name__':
    main()