'''
This module provides interface for work with library.
'''

from django.core.management import call_command
from django.test.utils import get_runner
from django.conf import settings

import getpass
import logging
import MySQLdb
import django
import sys
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tasktracker.settings")
django.setup()

from tasktracker.lib.logger import init_logger
import tasktracker.lib.actions.user_actions as user
import tasktracker.lib.actions.task_actions as task
import tasktracker.lib.actions.project_actions as project
import tasktracker.lib.actions.user_role_actions as user_role
import tasktracker.lib.actions.tag_actions as tag
import tasktracker.lib.actions.routine_actions as routine
import tasktracker.lib.actions.reminder_actions as reminder
import tasktracker.lib.actions.background_actions as background



def init(logger_config=None):
    init_logger(logger_config=logger_config)

def setup_database(database_config):
    db = None
    try:
        db = MySQLdb.connect(host=database_config['HOST'],
                             port=database_config['PORT'],
                             user=database_config['USER'],
                             passwd=database_config['PASSWORD'])
    except:
        logger.error('Can\'t connect to database')
        raise
    
    c = db.cursor()
    try:
        c.execute('create database {};'.format(database_config['DATABASE_NAME']))
    except:
        pass

    call_command('migrate')

    try:
        os.mkdir(os.path.join('/home', getpass.getuser(), '.ttrack_tracker'))
    except:
        pass

def run_tests():
    TestRunner = get_runner(settings)
    test_runner = TestRunner()
    failures = test_runner.run_tests(['tasktracker.lib.tests'])
    sys.exit(bool(failures))
