import tasktracker.lib.models as models
import tasktracker.lib.exceptions as exceptions
from tasktracker.lib.actions.background_actions import check_deadlines
from tasktracker.lib.logger import get_Logger

import logging

ROLES = {
    0: 'Organizer',
    1: 'Manager',
    2: 'Partner'
}

STATUSES = {
    0: 'InProgress',
    1: 'Pending',
    2: 'Done',
    3: 'Failed'
}

def complete_task(user, project_id, task_id):
    '''
    Action for any role.
    Marks task as completed, or pending if task is mentored.
    '''
    logger = get_Logger(__name__)
    ur = models.UserRole.objects.filter(project__id=project_id).filter(user__nickname=user)
    if not ur.exists():
        raise exceptions.ProjectAccessException('Project doesn\'t exist')
    project = ur[0].project
    task = project.task_set.get(id=task_id)
    check_deadlines(task.owner.nickname)
    if task.get_status_display() == STATUSES[0]:
        if task.mentor is None:
            task.status = 2
            task.save()
            logger.info(f'User {user} complete task {task_id} from project {project_id}')
            return 2
        else:
            task.status = 1
            task.save()
            logger.info(f'Task {task_id} from project {project_id} is pending the confirmation by mentor')
            return 1
    elif task.get_status_display() == STATUSES[1]:
        raise exceptions.NotAllowedException(f'Task is pending the confirmation by mentor.')
    elif task.get_status_display() == STATUSES[2]:
        raise exceptions.NotAllowedException('Task already completed.')
    else:
        raise exceptions.NotAllowedException('Task failed.')

def add_task_to_project(user, project_id, task_id):
    '''
    Only Owner and Manager action.
    Associates given task with given project.
    '''
    logger = get_Logger(__name__)
    task = models.User.objects.get(nickname=user).owner_tasks.get(id=task_id)
    ur = models.UserRole.objects.filter(project__id=project_id).filter(user__nickname=user)
    if not ur.exists():
        raise exceptions.ProjectAccessException('Project doesn\'t exist')
    project = ur[0].project
    if ur[0].get_role_display() == ROLES[0] or ur[0].get_role_display() == ROLES[1]:
        project.task_set.add(task)
        logger.info(f'User {user} added task {task_id} to project {project_id}')
    else:
        raise exceptions.ProjectAccessException('Partners don\'t have permissions to do that')

def remove_task_from_project(user, project_id, task_id):
    '''
    Only Owner and Manager action.
    Deassociates given task with given project.
    '''
    logger = get_Logger(__name__)
    ur = models.UserRole.objects.filter(project__id=project_id).filter(user__nickname=user)
    if not ur.exists():
        raise exceptions.ProjectAccessException('Project doesn\'t exist')
    project = ur[0].project
    task = project.task_set.get(id=task_id)
    if ur[0].get_role_display() == ROLES[0] or ur[0].get_role_display() == ROLES[1]:
        project.task_set.remove(task)
        logger.info(f'User {user} removed task {task_id} from project {project_id}')
    else:
        raise exceptions.ProjectAccessException('Partners don\'t have permissions to do that')

def invite_partner(user, new_partner, project_id):
    '''
    Only Owner and Manager action.
    Associates 'new_partner' with 'project_id' in the role of 'partner'.
    '''
    logger = get_Logger(__name__)
    ur = models.UserRole.objects.filter(project__id=project_id).filter(user__nickname=user)
    if not ur.exists():
        raise exceptions.ProjectAccessException('Project doesn\'t exist')
    if ur[0].get_role_display() == ROLES[2]:
        raise exceptions.ProjectAccessException('Partners can\'t invite other partners to project')
    new_prtnr = models.User.objects.get(nickname=new_partner)
    new_ur = models.UserRole.objects.filter(project__id=project_id).filter(user__nickname=new_prtnr)
    if new_ur.exists():
        raise exceptions.NotAllowedException(f'User {new_partner} is already a partner of project')
    models.UserRole(role=2, user=new_prtnr, project=ur[0].project).save()
    logger.info(f'User {new_partner} is now a partner of project {project_id}')

def exclude_partner(user, partner, project_id):
    '''
    Only Owner and Manager action.
    Deassociates 'partner' with 'project_id'.
    Manager can't exclude other Manager or Owner.
    '''
    logger = get_Logger(__name__)
    ur = models.UserRole.objects.filter(project__id=project_id).filter(user__nickname=user)
    if not ur.exists():
        raise exceptions.ProjectAccessException('Project doesn\'t exist')
    if ur[0].get_role_display() == ROLES[2]:
        raise exceptions.ProjectAccessException('Partners can\'t exclude other partners from project')
    prtnr = models.User.objects.get(nickname=partner)
    prtnr_ur = models.UserRole.objects.filter(project__id=project_id).filter(user__nickname=prtnr)
    if not prtnr_ur.exists():
        raise exceptions.NotAllowedException(f'User {partner} doesn\'t a partner of project')
    if prtnr_ur[0].get_role_display() == ROLES[0]:
        raise exceptions.ProjectAccessException('You can\'t exclude organizer of a project')
    elif prtnr_ur[0].get_role_display() == ROLES[1]:
        if ur[0].get_role_display() == ROLES[1]:
            raise exceptions.ProjectAccessException('Manager can\'t exclude other managers')
        else:
            prtnr_ur[0].delete()
            logger.info('User {partner} was excluded from project')
    else:
        prtnr_ur[0].delete()
        logger.info('User {partner} was excluded from project')

def appoint_as_manager(user, partner, project_id):
    '''
    Only Owner action.
    Gives Partner status of Project Manager.
    '''
    logger = get_Logger(__name__)
    ur = models.UserRole.objects.filter(project__id=project_id).filter(user__nickname=user)
    if not ur.exists():
        raise exceptions.ProjectAccessException('Project doesn\'t exist')
    else:
        ur = ur[0]
    if not ur.get_role_display() == ROLES[0]:
        raise exceptions.ProjectAccessException('Only organizer can appoint managers')
    partner_ur = models.UserRole.objects.filter(project__id=project_id).filter(user__nickname=partner)
    if not partner_ur.exists():
        raise exceptions.NotAllowedException(f'User {partner} not found')
    else:
        partner_ur = partner_ur[0]
    if partner_ur.get_role_display() == ROLES[1]:
        raise exceptions.NotAllowedException(f'User {partner} is already a manager.')
    else:
        partner_ur.role = 1
        partner_ur.save()
        logger.info(f'User {partner} is now a manager of project {project_id}')

def dismiss_manager(user, manager, project_id):
    '''
    Only Owner action.
    Removes from 'manager' role of Manager - he became Partner of Project.
    '''
    logger = get_Logger(__name__)
    ur = models.UserRole.objects.filter(project__id=project_id).filter(user__nickname=user)
    if not ur.exists():
        raise exceptions.ProjectAccessException('Project doesn\'t exist')
    else:
        ur = ur[0]
    if not ur.get_role_display() == ROLES[0]:
        raise exceptions.ProjectAccessException('Only organizer can dismiss managers')
    manager_ur = models.UserRole.objects.filter(project__id=project_id).filter(user__nickname=manager)
    if not manager_ur.exists():
        raise exceptions.NotAllowedException(f'User {manager} not found')
    else:
        manager_ur = manager_ur[0]
    if manager_ur.get_role_display() == ROLES[2]:
        raise exceptions.NotAllowedException(f'User {manager} is already not a manager.')
    else:
        manager_ur.role = 2
        manager_ur.save()
        logger.info(f'User {manager} is no longer a manager of project {project_id}')
    