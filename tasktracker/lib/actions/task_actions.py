'''
Provides actions with Tasks.
'''

import tasktracker.lib.models as models
import tasktracker.lib.exceptions as exceptions
from tasktracker.lib.actions.background_actions import check_deadlines
from tasktracker.lib.logger import get_Logger
import re
import logging
import datetime

PRIORITIES = {
    0: 'FarFuture',
    1: 'Low',
    2: 'Normal',
    3: 'High',
    4: 'Extremely'
}

STATUSES = {
    0: 'InProgress',
    1: 'Pending',
    2: 'Done',
    3: 'Failed'
}

def create_task(user, title, description='', priority=2, day=None, time=None):
    '''
    Creates new task.
    '''
    logger = get_Logger(__name__)
    if re.fullmatch('^\s*$', title):
        logger.error('Title can\'t be blank')
        raise ValueError('Title can\'t be blank')
    if len(title) > 100:
        logger.error('Title has max length 100')
        raise ValueError('Title has max length 100')
    if len(description) > 1000:
        logger.error('Description has max length 1000')
        raise ValueError('Description has max length 1000')
    if PRIORITIES.get(priority) is None:
        logger.error('Priority value can be only 0, 1, 2, 3 or 4.')
        raise ValueError('Priority value can be only 0, 1, 2, 3 or 4.')
    if day is not None and day < datetime.date.today():
            logger.error('Deadline can\'t be in past')
            raise ValueError('Deadline can\'t be in past')
    if time is not None:
        if  day is None:
            logger.error('You have to select day to select time')
            raise ValueError('You have to select day to select time')
        if (datetime.datetime.combine(day, time) <= datetime.datetime.now()):
            logger.error('Deadline can\'t be in past')
            raise ValueError('Deadline can\'t be in past')
    t = models.Task(title=title, description=description, priority=priority, status=0,
                    deadline_day=day, deadline_time=time, owner=models.User.objects.get(nickname=user))
    t.save()
    logger.info(f'User {user} created task {title}')
    return t.id

def update_task(user, id, title=None, description=None, priority=None, day=None, time=None):
    '''
    Updates some fields of given Task.
    '''
    logger = get_Logger(__name__)
    t = None
    try:
        t = models.User.objects.get(nickname=user).owner_tasks.get(id=id)
    except models.Task.DoesNotExist:
        logger.error(f'Task {id} doesn\'t exist')
        raise
    try:
        if title is not None:
            if not re.fullmatch('^\s*$', title) and len(title) <= 100:
                t.title = title
            else:
                raise ValueError('Wrong title.')
        if description is not None:
            if len(description) <= 1000:
                t.description = description
            else:
                raise ValueError('Description too long.')
        if priority is not None:
            if PRIORITIES.get(priority) is not None:
                t.priority = priority
            else:
                raise ValueError('Priority can be only 0, 1, 2, 3 or 4..')
        if day is not None:
            if day < datetime.date.today():
                raise ValueError('Deadline can\'t be in past')
            else:
                t.deadline_day = day
        if time is not None:
            if day is None:
                raise ValueError('You have select day to select time')
            elif (datetime.datetime.combine(day, time) <= datetime.datetime.now()):
                raise ValueError('Deadline can\'t be in past')
            else:
                t.deadline_time = time
    except ValueError:
        logger.error('Task update. ValueError')
        raise
    t.save()
    logger.info(f'User {user} updated task {id}')

def delete_task(user, id):
    '''
    Deletes Task with given id.
    '''
    logger = get_Logger(__name__)
    try:
        models.User.objects.get(nickname=user).owner_tasks.get(id=id).delete()
        logger.info(f'User {user} deleted task {id}')
    except models.Task.DoesNotExist:
        logger.error(f'User {user}. Task {id} doesn\'t exist')

def get_user_tasks(user):
    '''
    Returns all User Task's.
    '''
    tasks = models.User.objects.get(nickname=user).owner_tasks.all()
    res = []
    for t in tasks:
        if t.parent_task is None and t.project is None:
            res.append(t)
    return [(t.id, t.title, t.get_status_display()) for t in res]

def get_mentor_tasks(mentor):
    '''
    Returns all Tasks, that user mentored.
    '''
    tasks = models.User.objects.get(nickname=mentor).mentor_tasks.all()
    return [(t.id, t.title, t.get_status_display()) for t in tasks]

def get_tasks_by_status(user, status):
    '''
    Returns Task's with the given status.
    '''
    if STATUSES.get(status) is None:
        raise ValueError('Status value can be only 0, 1, 2 or 3')
    tasks = models.User.objects.get(nickname=user).owner_tasks.filter(status=status)
    return [(t.id, t.title, t.get_status_display()) for t in tasks]

def get_tasks_by_priority(user, priority):
    '''
    Returns Task's with the given priority.
    '''
    if PRIORITIES.get(priority) is None:
        raise ValueError('Status value can be only 0, 1, 2, 3 or 4')
    tasks = models.User.objects.get(nickname=user).owner_tasks.filter(priority=priority)
    return [(t.id, t.title, t.get_status_display()) for t in tasks]



def add_tag(user, id, tag):
    '''
    Add's Tag to given Task.
    '''
    logger = get_Logger(__name__)
    t = None
    try:
        t = models.User.objects.get(nickname=user).owner_tasks.get(id=id)
    except models.Task.DoesNotExist:
        logger.error(f'User {user}. Task {id} doesn\'t exist')
        raise
    if re.fullmatch('^\s*$', tag):
        logger.error('Tag name can\'t be blank')
        raise ValueError('Tag name can\'t be blank')
    if len(tag) > 20:
        logger.error('Tag name too long')
        raise ValueError('Tag name too long')
    try:
        t.tags.get(name=tag)
        logger.info(f'Task {id} already has tag {tag}')
        return
    except models.Tag.DoesNotExist:
        tagg, created = models.Tag.objects.get_or_create(name=tag)
        t.tags.add(tagg)
        logger.info(f'User {user} added tag {tag} to task {id}')

def delete_tag(user, id, tag):
    '''
    Remove association of tag with the given Task.
    '''
    logger = get_Logger(__name__)
    try:
        t = models.User.objects.get(nickname=user).owner_tasks.get(id=id)
        tg = t.tags.get(name=tag)
        t.tags.remove(tg)
        t.save()
        logger.error(f'User {user} removed tag {tag} from task {id}')
    except models.Task.DoesNotExist:
        logger.error(f'User {user}. Task {id} doesn\'t exist')
        raise

def get_tags(user, task_id):
    logger = get_Logger(__name__)
    task = None
    try:
        task = models.User.objects.get(nickname=user).owner_tasks.get(id=task_id)
    except models.Task.DoesNotExist:
        logger.error(f'User {user}. Task {id} doesn\'t exist')
        raise
    return [t.name for t in task.tags.all()]
    



def add_subtask(user, parent_task_id, subtask_id):
    '''
    Add's subtask 'subtask_id' to task 'parent_task_id'.
    '''
    logger = get_Logger(__name__)
    parent_task = None
    subtask = None
    try:
        parent_task = models.User.objects.get(nickname=user).owner_tasks.get(id=parent_task_id)
        subtask = models.User.objects.get(nickname=user).owner_tasks.get(id=subtask_id)
    except models.Task.DoesNotExist:
        logger.error(f'Task doesn\'t exist')
        raise
    subtask.parent_task = parent_task
    subtask.save()
    logger.info(f'Created subtask {subtask_id} for task {parent_task_id}')



def get_subtree(task):
    if task.task_set.exists():
        res = []
        for t in task.task_set.all():
            res.append(((t.id, t.title, t.get_status_display()), get_subtree(t)))
        return res
    else:
        return []

def get_task_tree(user, task_id):
    '''
    Retuns tree of tasks with parent task 'task_id'. In the next form:

        tree = ((parent_task_id, parent_task_title, parent_task_status), subtree)

    where 'subtreee' is list of subtrees in form of 'tree'.
    '''
    task = models.User.objects.get(nickname=user).owner_tasks.get(id=task_id)
    return ((task.id, task.title, task.get_status_display()), get_subtree(task))



def mark_as_completed(user, task_id):
    '''
    Marks task as completed, or pending if task is mentored.
    '''
    logger = get_Logger(__name__)
    check_deadlines(user)
    task = None
    try:
        task = models.User.objects.get(nickname=user).owner_tasks.get(id=task_id)
    except models.Task.DoesNotExist:
        logger.error(f'Task {task_id} doesn\'t exist')
        raise
    if task.get_status_display() == STATUSES[0]:
        if task.mentor is None:
            task.status = 2
            task.save()
            logger.info(f'User {user} complete task {task_id}')
            return 2
        else:
            task.status = 1
            task.save()
            logger.info(f'Task {task_id} of user {user} is pending the confirmation by mentor')
            return 1
    elif task.get_status_display() == STATUSES[1]:
        raise exceptions.NotAllowedException(f'Task is pending the confirmation by mentor.')
    elif task.get_status_display() == STATUSES[2]:
        raise exceptions.NotAllowedException('Task already completed.')
    else:
        raise exceptions.NotAllowedException('Task has been failed before.')
    
def invite_mentor(user, task_id, mentor_nickname):
    '''
    Associates mentor 'mentor_nickname' with Task 'task_id'.
    '''
    logger = get_Logger(__name__)
    task = None
    mentor = None
    try:
        task = models.User.objects.get(nickname=user).owner_tasks.get(id=task_id)
        mentor = models.User.objects.get(nickname=mentor_nickname)
    except models.User.DoesNotExist:
        logger.error(f'User {mentor_nickname} doesn\'t exist')
        raise
    except models.Task.DoesNotExist:
        logger.error(f'Task {task_id} doesn\'t exist')
        raise
    task.mentor = mentor
    task.save()
    logger.info(f'User {mentor_nickname} is now mentor of user {user} for task {task_id}')

def mentor_confirm_completion(mentor, task_id):
    '''
    Action for mentor.
    Confirms completion of associated task if 'task_id' in 'pended' status.
    '''
    logger = get_Logger(__name__)
    task = None
    try:
        task = models.User.objects.get(nickname=mentor).mentor_tasks.get(id=task_id)
    except models.Task.DoesNotExist:
        logger.error(f'Task {task_id} doesn\'t exist')
        raise
    if task.get_status_display() == STATUSES[1]:
        task.status = 2
        task.save()
        logger.info(f'Mentor confirmed completion of task {task_id} by user {task.owner}')
    elif task.get_status_display() == STATUSES[0]:
        raise exceptions.NotAllowedException('Owner hasn\'t complete task yet')
    elif task.get_status_display() == STATUSES[2]:
        raise exceptions.NotAllowedException('Task already completed')
    else:
        raise exceptions.NotAllowedException('Owner already has failed task')

def mentor_mark_failed(mentor, task_id):
    '''
    Action for mentor.
    Fails completion of associated task if 'task_id' in 'pended' status.
    '''
    logger = get_Logger(__name__)
    task = None
    try:
        task = models.User.objects.get(nickname=mentor).mentor_tasks.get(id=task_id)
    except models.Task.DoesNotExist:
        logger.error(f'Task {task_id} doesn\'t exist')
        raise
    if task.get_status_display() == STATUSES[1]:
        task.status = 3
        task.save()
        logger.info(f'Mentor marked task {task_id} as failed by user {task.owner}')
    elif task.get_status_display() == STATUSES[0]:
        raise exceptions.NotAllowedException('Owner hasn\'t finished task yet')
    elif task.get_status_display() == STATUSES[2]:
        raise exceptions.NotAllowedException('Task completed')
    else:
        raise exceptions.NotAllowedException('Owner has failed task')



def get_task_info(user, id):
    '''
    Returns all Fields of given task.
    '''
    logger = get_Logger(__name__)
    check_deadlines(user)
    t = None
    try:
        t = models.User.objects.get(nickname=user).owner_tasks.filter(id=id).values()
    except models.Task.DoesNotExist:
        try:
            t = models.User.objects.get(nickname=user).mentor_tasks.filter(id=id).values()
        except models.Task.DoesNotExist:    
            logger.error(f'User {user}. Task {id} doesn\'t exist')
            raise
    return list(t)[0]

def task_exists(user, id):
    '''
    Returns True if Task exists, and False if not.
    '''
    return True if models.User.objects.get(nickname=user).owner_tasks.filter(id=id).exists() else False
