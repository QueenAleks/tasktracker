import logging
import os

LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': 'DEBUG',
        'handlers': [
            'file',
        ]
    },
    'formatters': {
        'default': {
            'format': '%(asctime)s - %(name)s - %(lineno)s - %(levelname)s - %(message)s'
        }
    },
    'handlers': {
        'file': {
            'class': 'logging.FileHandler',
            'formatter': 'default',
            'level': 'DEBUG',
            'filename': 'tt_log.log'
        }
    }
}

DATABASE_CONFIG = {
    'DATABASE_NAME': 'ttrack_cool_base',
    'USER': 'root',
    'PASSWORD': 'password',
    'HOST': 'localhost',
    'PORT': 3306
}