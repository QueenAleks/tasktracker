from django.test import TestCase

import tasktracker.lib.actions.project_actions as project
import tasktracker.lib.exceptions as exceptions
import tasktracker.lib.models as models

class ProjectActionsTestCase(TestCase):
    def setUp(self):
        u1 = models.User(nickname='kek')
        u1.save()
        self.pr = models.Project(name='test1')
        self.pr.save()
        self.ur = models.UserRole(role=0, user=u1, project=self.pr)
        self.ur.save()
        self.task1 = models.Task(title='test1', owner=u1, project=self.pr)
        self.task1.save()

    def test_create_project(self):
        project.create_project('kek', 'test2')
        self.assertIn(models.User.objects.get(nickname='kek').project_set.get(name='test2'),
                      models.Project.objects.all())
        with self.assertRaises(ValueError):
            project.create_project('kek', '   ')
            project.create_project('kek', 's'*150)

    def test_delete_project(self):
        project.delete_project('kek', self.pr.id)
        self.assertFalse(models.User.objects.get(nickname='kek').project_set.all().exists())

    def test_get_user_projects(self):
        res = project.get_user_projects('kek')
        self.assertEqual(res, [(self.pr.id, self.pr.name, 'Organizer')])
    
    def test_get_projects_by_role(self):
        res = project.get_projects_by_role('kek', 0)
        self.assertEqual(res, [(self.pr.id, self.pr.name)])

    def test_get_project_tasks(self):
        res = project.get_project_tasks('kek', self.pr.id)
        self.assertEqual(res, [(self.task1.id, self.task1.title, 'InProgress')])

    def test_get_members(self):
        res = project.get_members(self.pr.id)
        self.assertEqual(res, [('kek', 'Organizer')])
