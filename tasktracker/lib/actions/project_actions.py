'''
Provides actions with Projects.
'''

import tasktracker.lib.models as models
import tasktracker.lib.exceptions as exceptions
from tasktracker.lib.logger import get_Logger

import logging
import re

ROLES = {
    0: 'Organizer',
    1: 'Manager',
    2: 'Partner'
}

def create_project(user, project_name):
    '''
    Creates new project. 'user' becomes it's owner.
    '''
    logger = get_Logger(__name__)
    if re.fullmatch('^\s*$', project_name):
        logger.error('Project name can\'t be blank')
        raise ValueError('Project name can\'t be blank')
    if len(project_name) > 100:
        logger.error('Project name has max length 100')
        raise ValueError('Project name has max length 100')
    u = models.User.objects.get(nickname=user)
    p = models.Project(name=project_name)
    p.save()
    r = models.UserRole(role=0, user=u, project=p)
    r.save()
    logger.info(f'User {user} created project {project_name}')
    return p.id

def rename_project(user, id, project_name):
    '''
    Only Owner action.
    Renames given project.
    '''
    logger = get_Logger(__name__)
    project = None
    try:
        project = models.Project.objects.get(id=id)
    except models.Project.DoesNotExist:
        logger.error(f'Project {id} doesn\'t exist')
        raise
    if re.fullmatch('^\s*$', project_name):
        logger.error('Project name can\'t be blank')
        raise ValueError('Project name can\'t be blank')
    if len(project_name) > 100:
        logger.error('Project name has max length 100')
        raise ValueError('Project name has max length 100')
    if models.UserRole.objects.filter(user=user).filter(project__id=project.id)[0].get_role_display() == ROLES[0]:
        project.name = project_name
        project.save()
        logger.info(f'Project {id} has been renamed')
    else:
        raise exceptions.ProjectAccessException('Only organizer can rename project')

def delete_project(user, id):
    '''
    Only Owner action.
    Deletes given project with all it's tasks and associations with other users.
    '''
    logger = get_Logger(__name__)
    project = None
    try:
        project = models.Project.objects.get(id=id)
    except models.Project.DoesNotExist:
        logger.error(f'Project {id} doesn\'t exist')
    ur = models.UserRole.objects.filter(project=project).filter(user__nickname=user)
    if not ur.exists():
        raise exceptions.ProjectAccessException('User doesn\'t have access to this project')
    if ur[0].get_role_display() == ROLES[0]:
        project.delete()
        logger.info(f'Project {id} has been deleted')
    else:
        raise exceptions.ProjectAccessException('Only organizer can delete project')

def get_project_role(user, project_id):
    ur = models.UserRole.objects.filter(project__id=project_id).filter(user__nickname=user)
    return ur[0].get_role_display()

def get_user_projects(user):
    '''
    Returns all projects, thats has assotiations with current user with any role.
    '''
    projects = models.User.objects.get(nickname=user).project_set.all()
    return [(p.id, p.name, get_project_role(user, p.id)) for p in projects]

def get_projects_by_role(user, role):
    '''
    Returns projects in which 'user' has given role.
    '''
    if ROLES.get(role) is None:
        raise ValueError('Role value can be only 0, 1 or 2')
    user_roles = models.UserRole.objects.filter(user__nickname=user).filter(role=role).prefetch_related('project')
    return [(ur.project.id, ur.project.name) for ur in user_roles]

def get_project_tasks(user, project_id):
    '''
    Returns all tasks of given project.
    '''
    ur = models.UserRole.objects.filter(project__id=project_id).filter(user__nickname=user)
    if not ur.exists():
        raise exceptions.ProjectAccessException('Project doesn\'t exist')
    tasks = ur[0].project.task_set.all()
    return [(t.id, t.title, t.get_status_display()) for t in tasks]

def get_members(project_id):
    '''
    Returns all of given project member's nicknames.
    '''
    members = []
    ur = models.UserRole.objects.filter(project__id=project_id).order_by('role')
    for u in ur:
        members.append((u.user.nickname, u.get_role_display()))
    return members
