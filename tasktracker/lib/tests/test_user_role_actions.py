from django.test import TestCase

import tasktracker.lib.actions.user_role_actions as user_role
import tasktracker.lib.exceptions as exceptions
import tasktracker.lib.models as models

class UserRoleActionsTestCase(TestCase):
    def setUp(self):
        u1 = models.User(nickname='kek')
        u1.save()
        self.pr = models.Project(name='test1')
        self.pr.save()
        self.ur = models.UserRole(role=0, user=u1, project=self.pr)
        self.ur.save()
        self.task1 = models.Task(title='test1', owner=u1, project=self.pr)
        self.task1.save()
        self.task2 = models.Task(title='test2', owner=u1)
        self.task2.save()
        u2 = models.User(nickname='lol')
        u2.save()
        models.UserRole(role=2, user=u2, project=self.pr).save()
        models.User(nickname='che').save()
        u3 = models.User(nickname='man')
        u3.save()
        models.UserRole(role=1, user=u3, project=self.pr).save()

    def test_complete_task(self):
        user_role.complete_task('kek', self.pr.id, self.task1.id)
        self.assertEqual(models.Task.objects.get(title='test1').status, 2)

    def test_add_task_to_project(self):
        user_role.add_task_to_project('kek', self.pr.id, self.task2.id)
        self.assertEqual(models.Task.objects.get(title='test2').project, self.pr)

    def test_remove_task_from_project(self):
        user_role.remove_task_from_project('kek', self.pr.id, self.task1.id)
        self.assertIsNone(models.Task.objects.get(title='test1').project)

    def test_invite_partner(self):
        user_role.invite_partner('kek', 'che', self.pr.id)
        self.assertEqual(models.UserRole.objects.filter(project=self.pr).filter(user__nickname='che').all()[0].role, 2)

    def test_exclude_partner(self):
        user_role.exclude_partner('kek', 'lol', self.pr.id)
        self.assertFalse(self.pr.users.filter(nickname='lol').exists())

    def test_appoint_as_manager(self):
        user_role.appoint_as_manager('kek', 'lol', self.pr.id)
        self.assertEqual(models.UserRole.objects.filter(project=self.pr).filter(user__nickname='lol').all()[0].role, 1)

    def test_dismiss_manager(self):
        user_role.dismiss_manager('kek', 'man', self.pr.id)
        self.assertEqual(models.UserRole.objects.filter(project=self.pr).filter(user__nickname='man').all()[0].role, 2)
