from tasktracker.lib.core import background

def do_background_jobs(func):
    def wrapper(request, **kwargs):
        background.make_routine_tasks()
        return func(request, **kwargs)
    return wrapper