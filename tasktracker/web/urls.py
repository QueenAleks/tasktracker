"""web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from tasktracker.web.views import (
    user_views,
    task_views,
    project_views
)

urlpatterns = [
    url('register/', user_views.register, name='register'),
    url('login/', user_views.login_view, name='login'),
    url('logout/', user_views.logout_view, name='logout'),

    url(r'^user/(?P<nickname>[a-zA-Z0-9]+)/$', user_views.user_page, name='user_page'),
    url(r'^user/(?P<nickname>[a-zA-Z0-9]+)/delete/$', user_views.user_delete, name='user_delete'),

    url(r'^task/(?P<task_id>[0-9]+)/$', task_views.task_page, name='task_page'),
    url(r'^task/(?P<task_id>[0-9]+)/delete/$', task_views.task_delete, name='task_delete'),
    url('task/create/', task_views.task_create, name='task_create'),
    url(r'^task/add_subtask/(?P<parent_task_id>[0-9]+)/$', task_views.task_add_subtask, name='task_add_subtask'),
    url(r'^task/(?P<task_id>[0-9]+)/add_tag/$', task_views.task_add_tag, name='task_add_tag'),
    url(r'^task/remove_tag/(?P<task_id>[0-9]+)/(?P<tag>[a-zA-Z0-9]+)/$', task_views.task_remove_tag, name='task_remove_tag'),
    url(r'^task/complete/(?P<task_id>[0-9]+)/$', task_views.task_complete, name='task_complete'),
    url('task/invite_mentor/', task_views.task_invite_mentor, name='task_invite_mentor'),
    url(r'^task/select_period/(?P<task_id>[0-9]+)/$', task_views.task_select_period, name='task_select_period'),
    url('task/create_reminder/', task_views.task_create_reminder, name='task_create_reminder'),
    url('task/delete_reminder/', task_views.task_delete_reminder, name='task_delete_reminder'),
    url('mentor/confirm/', task_views.mentor_confirm, name='mentor_confirm'),
    url('mentor/mark_failed/', task_views.mentor_mark_failed, name='Mentor_mark_failed'),

    url(r'^project/(?P<project_id>[0-9]+)/$', project_views.project_page, name='project_page'),
    url(r'^project/(?P<project_id>[0-9]+)/delete/$', project_views.project_delete, name='project_delete'),
    url('project/create/', project_views.project_create, name='project_create'),
    url(r'^project/complete_task/(?P<project_id>[0-9]+)/(?P<task_id>[0-9]+)/$', project_views.project_complete_task, name='project_complete_task'),
    url(r'^project/add_task/(?P<project_id>[0-9]+)/$', project_views.project_add_task, name='project_add_task'),
    url(r'^project/remove_task/(?P<project_id>[0-9]+)/(?P<task_id>[0-9]+)/$', project_views.project_remove_task, name='project_remove_task'),
    url(r'^project/invite/(?P<project_id>[0-9]+)/$', project_views.project_invite, name='project_invite'),
    url(r'^project/exclude/(?P<project_id>[0-9]+)/(?P<nickname>[a-zA-Z0-9]+)/$', project_views.project_exclude, name='project_exclude'),
    url(r'^project/appoint_manager/(?P<project_id>[0-9]+)/(?P<nickname>[a-zA-Z0-9]+)/$', project_views.project_appoint_manager, name='project_appoint_manager'),
    url(r'^project/dismiss_manager/(?P<project_id>[0-9]+)/(?P<nickname>[a-zA-Z0-9]+)/$', project_views.project_dismiss_manager, name='project_dismiss_manager'),

    url(r'^$', user_views.home_page),
    url(r'^admin/', admin.site.urls),
]
