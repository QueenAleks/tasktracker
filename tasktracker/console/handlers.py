import tasktracker.lib.core as core
from tasktracker.console.session import Session

import datetime
import dateutil
import dateutil.parser

def login_required(func):
    def wrapper(args, user):
        if user is None:
            print('You need to login first.')
            return
        func(args, user)
    return wrapper



def create_user(args, user):
    try:
        core.user.create_user(name=args.nickname)
    except Exception as ex:
        for arg in ex.args:
            print(arg)
        return
    print(f'Created account of user {args.nickname}.')

@login_required
def delete_user(args, user):
    print('''
        Are you sure?
        All your data will be lost, and you will not be able to restore it!
        
        Print your nickname to confirm:
    ''')
    confirmation = input()
    print()
    if confirmation != user:
        print('This is not your nickname. Abort.')
        return
    core.user.delete_user(name=user)
    print(f'User {user} was deleted.')
    return Session()

def login(args, user):
    if not core.user.user_exists(args.nickname):
        print(f'Account {args.nickname} doesn\'t exist')
    else:
        new_session = Session(args.nickname)
        print(f'Logined as {args.nickname}')
        return new_session

@login_required
def whoami(args, user):
    print(f'Current user is {user}')

@login_required
def get_tags(args, user):
    for tag in core.tag.get_tags(user):
        print(tag)

@login_required
def delete_tag(args, user):
    core.tag.delete_tag(user, args.tag)
    print(f'Tag {args.tag} was deleted.')



class task:
    PRIORITIES = {
        'FarFuture': 0,
        'Low': 1,
        'Normal': 2,
        'High': 3,
        'Extremely': 4
    }

    STATUSES = {
        'InProgress': 0,
        'Pending': 1,
        'Done': 3,
        'Failed': 4
    }

    @staticmethod
    @login_required
    def create(args, user):
        new_task_id = 0
        try:
            day = None if args.date is None else dateutil.parser.parse(args.date).date()
            time = None if args.time is None else dateutil.parser.parse(args.time).time()
            new_task_id = core.task.create_task(
                user,
                args.title,
                description=args.description,
                priority=task.PRIORITIES[args.priority],
                day=day,
                time=time
            )
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'Created task #{new_task_id}: {args.title}')

    @staticmethod
    @login_required
    def update(args, user):
        try:
            core.task.update_task(
                user,
                args.task_id,
                title=args.title,
                description=args.description,
                priority=None if args.priority is None else task.PRIORITIES[args.priority],
                day=None if args.date is None else dateutil.parser.parse(args.date).date(),
                time=None if args.time is None else dateutil.parser.parse(args.time).time()
            )
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'Task #{args.task_id} was sucsessfully updated.')

    @staticmethod
    @login_required
    def delete(args, user):
        try:
            core.task.delete_task(user, args.task_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'Deleted task {args.task_id}.')

    @staticmethod
    @login_required
    def get_all(args, user):
        for task_id, task_title, status in core.task.get_user_tasks(user):
            print(f'#{task_id} {task_title} - {status}')

    @staticmethod
    @login_required
    def get_info(args, user):
        res = None
        try:
            res = core.task.get_task_info(user, args.task_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        for key, value in res.items():
            print(f'{key}: {value}')

    @staticmethod
    @login_required
    def find_by_priority(args, user):
        for task_id, task_title in core.task.get_tasks_by_priority(user, task.PRIORITIES[args.priority]):
            print(f'#{task_id} {task_title}')

    @staticmethod
    @login_required
    def find_by_status(args, user):
        for task_id, task_title in core.task.get_tasks_by_status(user, task.STATUSES[args.status]):
            print(f'#{task_id} {task_title}')

    @staticmethod
    @login_required
    def find_by_tag(args, user):
        for task_id, task_title in core.tag.get_tasks_by_tag(user, task.STATUSES[args.tag]):
            print(f'#{task_id} {task_title}')

    @staticmethod
    @login_required
    def add_tag(args, user):
        try:
            core.task.add_tag(user, args.task_id, args.tag)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'Added tag {args.tag} to task #{args.task_id}')

    @staticmethod
    @login_required
    def remove_tag(args, user):
        try:
            core.task.delete_tag(user, args.task_id, args.tag)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'Deleted tag {args.tag} from task #{args.task_id}')

    @staticmethod
    @login_required
    def add_subtask(args, user):
        try:
            core.task.add_subtask(user, args.task_id, args.subtask_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'Task #{args.subtask_id} is now a subtask of task #{args.task_id}')


    @staticmethod
    def print_subtree(node, level):
        current, subtasks = node
        id, title, status = current
        s = '    ' * level
        print(f'{s}#{id}: {title} - {status}')
        for n in subtasks:
            task.print_subtree(n, level + 1)

    @staticmethod
    @login_required
    def get_task_tree(args, user):
        par, subtasks = core.task.get_task_tree(user, int(args.task_id))
        id, title, status = par
        print(f'#{id}: {title} - {status}')
        for node in subtasks:
            task.print_subtree(node, 1)

    @staticmethod
    @login_required
    def invite_mentor(args, user):
        try:
            core.task.invite_mentor(user, args.task_id, args.nickname)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'User {args.nickname} now mentors task #{args.task_id}')

    @staticmethod
    @login_required
    def complete(args, user):
        result = 0
        try:
            result = core.task.mark_as_completed(user, args.task_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        if result == 1:
            print(f'OK! Now mentor have to confirm you completed the task.')
        elif result == 2:
            print(f'Task #{args.task_id} completed.')



class mentor:
    @staticmethod
    @login_required
    def confirm(args, user):
        try:
            core.task.mentor_confirm_completion(user, args.task_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'You confirmed task #{args.task_id}.')
    
    @staticmethod
    @login_required
    def mark_failed(args, user):
        try:
            core.task.mentor_mark_failed(user, args.task_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'You mark task #{args.task_id} as failed.')

    @staticmethod
    @login_required
    def get_info(args, user):
        res = None
        try:
            res = core.task.get_task_info(user, args.task_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        for key, value in res:
            print(f'{key}: {value}')

    @staticmethod
    @login_required
    def get_tasks(args, user):
        for task_id, task_title in core.task.get_mentor_tasks(user):
            print(f'#{task_id} {task_title}')
        



class routine:
    PERIODS = {
        'Daily': 0,
        'Weekly': 1,
        'Monthly': 2,
        'Yearly': 3
    }

    @staticmethod
    @login_required
    def create(args, user):
        try:
            core.routine.create_routine(user, args.task_id, routine.PERIODS[args.period])
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'Task #{args.task_id} will be repeated {args.period}.')

    @staticmethod
    @login_required
    def delete(args, user):
        try:
            core.routine.delete_routine(user, args.task_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'Task {args.task_id} will no longer be repeated.')



class reminder:
    @staticmethod
    @login_required
    def create(args, user):
        new_reminder_id = 0
        try:
            new_reminder_id = core.reminder.create_reminder(
                user,
                args.task_id,
                dateutil.parser.parse(args.datetime),
                args.note
            )
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'Reminder #{new_reminder_id} will be triggered at {args.datetime}')

    @staticmethod
    @login_required
    def delete(args, user):
        try:
            core.reminder.delete_reminder(user, args.reminder_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'Deleted reminder #{args.reminder_id}')

    @staticmethod
    @login_required
    def get_all(args, user):
        for task_id, at_time, note in core.background.get_reminders(user):
            print(f'Task #{task_id}: {at_time}: {note}')



class project:
    ROLES = {
        'Organizer': 0,
        'Manager': 1,
        'Partner': 2
    }

    @staticmethod
    @login_required
    def create(args, user):
        new_project_id = 0
        try:
            new_project_id = core.project.create_project(user, args.name)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'Created project #{new_project_id}')

    @staticmethod
    @login_required
    def rename(args, user):
        try:
            core.project.rename_project(user, args.project_id, args.name)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'You renamed project #{args.project_id}')

    @staticmethod
    @login_required
    def delete(args, user):
        print('''
            Are you sure?
            All members will lost access to their task and data.
            You will not be able to restore it.
            
            Print \'y\' to confirm.''')
        if input() != 'y':
            print('Abort.')
            return
        print()
        try:
            core.project.delete_project(user, args.project_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'You deleted project #{args.project_id}')

    @staticmethod
    @login_required
    def get_all(args, user):
        for project_id, project_name, project_role in core.project.get_user_projects(user):
            print(f'#{project_id}: {project_name} - {project_role}')

    @staticmethod
    @login_required
    def find_by_role(args, user):
        res = None
        try:
            res = core.project.get_projects_by_role(user, project.ROLES[args.role])
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        for project_id, project_name in res:
            print(f'#{project_id}: {project_name}')
        
    @staticmethod
    @login_required
    def get_tasks(args, user):
        res = None
        try:
            res = core.project.get_project_tasks(user, args.project_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        for task_id, task_title in res:
            print(f'#{task_id}: {task_title}')

    @staticmethod
    @login_required
    def get_members(args, user):
        for nickname, role in core.project.get_members(args.project_id):
            print(f'{nickname}: {role}')

    @staticmethod
    @login_required
    def complete_task(args, user):
        answer = 0
        try:
            answer = core.user_role.complete_task(user, args.project_id, args.task_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        if answer == 2:
            print(f'You complete task #{args.task_id}')
        elif answer == 1:
            print(f'OK! Now task has to be confirmed by it\'s mentor')

    @staticmethod
    @login_required
    def add_task(args, user):
        try:
            core.user_role.add_task_to_project(user, args.project_id, args.task_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'You added task #{args.task_id} to project #{args.project_id}')

    @staticmethod
    @login_required
    def remove_task(args, user):
        try:
            core.user_role.remove_task_from_project(user, args.project_id, args.task_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'You removed task #{args.task_id} from project #{args.project_id}')

    @staticmethod
    @login_required
    def invite_member(args, user):
        try:
            core.user_role.invite_partner(user, args.nickname, args.project_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'User {args.nickname} now is a partner in project #{args.project_id}')

    @staticmethod
    @login_required
    def exclude_member(args, user):
        try:
            core.user_role.exclude_partner(user, args.nickname, args.project_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'You excluded user {args.nickname} from project #{args.project_id}')

    @staticmethod
    @login_required
    def appoint_manager(args, user):
        try:
            core.user_role.appoint_as_manager(user, args.nickname, args.project_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'You appoint user {args.nickname} manager in project #{args.project_id}')

    @staticmethod
    @login_required
    def dismiss_manager(args, user):
        try:
            core.user_role.dismiss_manager(user, args.nickname, args.project_id)
        except Exception as ex:
            for arg in ex.args:
                print(arg)
            return
        print(f'You dismiss user {args.nickname} from being manager in project #{args.project_id}')
