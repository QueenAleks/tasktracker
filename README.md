#TTrack
TTrack is an app, that can help you to plan your day, increase your effectivity and make your dreams come true!!!
It can be used as a console tool or via web interface.
Written in Python3 language using Django framework.

##Installation
Firstly, download the repository:

	git clone https://bitbucket.org/QueenAleks/tasktracker

Than go to directory and type:

	sudo pip3 install .

After that, you will be able to use TTrack by the keyword 'ttrack'.

##Usage
You can learn more about operations on each of entities type by typing --help, for example:

	ttrack --help

##Development
After installation you also can use core lib of the application to develop your own python3 applications:

	import ttrack.lib.core
    
Explore core library by typing in python interpreter:

	help(ttrack.lib.core)

##Web interface    
You can use application via web interface by typing:

    python3 manage.py runserver

and go to localhost:8000 in your browser.

Or you can deploy it to your own server using your favourite (apache, nginx, etc.) and use it anywhere over the Internet