from django.test import TestCase

import tasktracker.lib.actions.task_actions as task
import tasktracker.lib.exceptions as exceptions
import tasktracker.lib.models as models

from dateutil.relativedelta import relativedelta
import datetime

class TaskActionsTestCase(TestCase):
    def setUp(self):
        u1 = models.User(nickname='kek')
        u1.save()
        self.task1 = models.Task(title='test1', owner=u1)
        self.task1.save()
        u2 = models.User(nickname='lol')
        u2.save()
        self.task2 = models.Task(title='test2', owner=u2, mentor=u1)
        self.task2.save()
        t, c = models.Tag.objects.get_or_create(name='ggg')
        self.task2.tags.add(t)
        self.task3 = models.Task(title='test3', owner=u2)
        self.task3.save()
        self.ment = models.User(nickname='ment')
        self.ment.save()

    def test_creation(self):
        t_id = task.create_task('kek', 'privet')
        self.assertIn(models.Task.objects.get(id=t_id), models.User.objects.get(nickname='kek').owner_tasks.all())

    def test_creation__errors(self):
        with self.assertRaises(ValueError):
            task.create_task('kek', '   ')
            task.create_task('kek', 't'*150)
            task.create_task('kek', 't', description='d'*1100)
            task.create_task('kek', 't', priority=6)
            task.create_task('kek', 't', day=datetime.datetime.now().day()+relativedelta(days=-1))
            task.create_task('kek', 't', time=datetime.datetime.now().time())
            task.create_task('kek', 't', day=datetime.datetime.now().day(), time=datetime.datetime.now().time()+relativedelta(hours=-1))

    def test_delete(self):
        task.delete_task('kek', self.task1.id)
        self.assertFalse(models.User.objects.get(nickname='kek').owner_tasks.all().exists())

    def test_get_user_tasks(self):
        self.assertEqual(task.get_user_tasks('kek'), [(self.task1.id, self.task1.title, 'InProgress')])

    def test_get_mentor_tasks(self):
        self.assertEqual(task.get_mentor_tasks('kek'), [(self.task2.id, self.task2.title, 'InProgress')])

    def test_get_task_by_status(self):
        self.assertEqual(task.get_tasks_by_status('kek', 0), [(self.task1.id, self.task1.title, 'InProgress')])

    def test_get_task_by_priority(self):
        self.assertEqual(task.get_tasks_by_priority('kek', 2), [(self.task1.id, self.task1.title, 'InProgress')])

    def test_add_tag(self):
        task.add_tag('kek', self.task1.id, 'tag1')
        self.assertTrue( self.task1.tags.filter(name='tag1').exists())
        with self.assertRaises(ValueError):
            task.add_tag('kek', self.task1.id, '    ')
            task.add_tag('kek', self.task1.id, 't'*30)
        with self.assertRaises(models.Task.DoesNotExist):
            task.add_tag('kek', -1, 'new')

    def test_delete_tag(self):
        task.delete_tag('lol', self.task2.id, 'ggg')
        self.assertFalse(self.task2.tags.all().exists())

    def test_add_subtask(self):
        task.add_subtask('lol', self.task2.id, self.task3.id)
        self.assertEqual(models.Task.objects.get(title='test3').parent_task, self.task2)
        with self.assertRaises(models.Task.DoesNotExist):
            task.add_subtask('lol', self.task2.id, -1)
            task.add_subtask('lol', -1, self.task3.id)
            task.add_subtask('lol', -1, -1)

    def test_mark_as_completed(self):
        ans1 = task.mark_as_completed('kek', self.task1.id)
        ans2 = task.mark_as_completed('lol', self.task2.id)
        self.assertEqual(ans1, 2)
        self.assertEqual(ans2, 1)

    def test_invite_mentor(self):
        task.invite_mentor('kek', self.task1.id, 'ment')
        self.assertEqual(models.Task.objects.get(title='test1').mentor, self.ment)
