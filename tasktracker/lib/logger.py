import logging

def get_Logger(name):
    return logging.getLogger(name)

def init_logger(logger_config=None):
    if logger_config is None:
        logging.disable(logging.CRITICAL)
    else:
        logging.config.dictConfig(logger_config)