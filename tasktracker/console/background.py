from tasktracker.lib.core import background as jobs

def do_jobs(user):
    jobs.make_routine_tasks()

    fails = jobs.check_deadlines(user)
    if len(fails) != 0:
        print('Notification! You did not complete task(s) on time!')
        print('Failed tasks:')
        for fail_id, fail_title in fails:
            print(f'    #{fail_id}: {fail_title}')
        print()

    reminders = jobs.get_reminders(user)
    if len(reminders) != 0:
        print(f'Notification! You have {len(reminders)} reminder(s)!')
        print('Use \'ttrack reminder get_all\' to see it.')
        print()