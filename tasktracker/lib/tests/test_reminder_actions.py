from django.test import TestCase

import tasktracker.lib.actions.reminder_actions as reminder
import tasktracker.lib.exceptions as exceptions
import tasktracker.lib.models as models

import datetime

class ReminderActionsTestCase(TestCase):
    def setUp(self):
        u = models.User(nickname='kek')
        u.save()
        self.task1 = models.Task(title=f'test1', owner=u)
        self.task1.save()
        r = models.Reminder(at_time=datetime.datetime.now(), task=self.task1)
        r.save()
        self.reminder_id = r.id

    def test_creation(self):
        reminder.create_reminder('kek', self.task1.id, datetime.datetime.now(), note='test creation')
        self.assertIn(models.Reminder.objects.get(note='test creation'), models.Reminder.objects.all())

    def test_creation__task_does_not_exist(self):
        with self.assertRaises(models.Task.DoesNotExist):
            reminder.create_reminder('kek', -1, datetime.datetime.now())

    def test_delete(self):
        reminder.delete_reminder('kek', self.reminder_id)
        self.assertFalse(self.task1.reminder_set.all().exists())