from django.test import TestCase

import tasktracker.lib.actions.user_actions as user
import tasktracker.lib.exceptions as exceptions
import tasktracker.lib.models as models

class UserActionsTestCase(TestCase):
    def setUp(self):
        models.User(nickname='kek').save()

    def test_creation(self):
        user.create_user(name='test1')
        self.assertIn(models.User.objects.get(nickname='test1'), models.User.objects.all())
    
    def test_creation__blank_nickname(self):
        with self.assertRaises(ValueError):
            user.create_user(name='    ')
    
    def test_creation__long_nickname(self):
        with self.assertRaises(ValueError):
            user.create_user(name='sadkgghdsakjgldskjkjgasjbgaslkdfjaslkdjfbalshbgljsakgljskdjg')

    def test_creation__user_exists(self):
        with self.assertRaises(exceptions.NotAllowedException):
            user.create_user(name='kek')
    
    def test_delete(self):
        user.delete_user(name='kek')
        with self.assertRaises(models.User.DoesNotExist):
            models.User.objects.get(nickname='kek')

    def test_user_exists__true(self):
        self.assertTrue(user.user_exists('kek'))
    
    def test_user_exists__false(self):
        self.assertFalse(user.user_exists('chpok'))